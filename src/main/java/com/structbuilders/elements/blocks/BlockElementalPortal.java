package com.structbuilders.elements.blocks;

import com.google.common.base.Predicates;
import com.structbuilders.elements.Elements;
import com.structbuilders.elements.Reference;
import com.structbuilders.elements.dimensions.elemental_junction.DimensionElementalJunction;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.pattern.BlockPattern;
import net.minecraft.block.pattern.BlockPatternBuilder;
import net.minecraft.block.pattern.BlockStateMatcher;
import net.minecraft.entity.Entity;
import net.minecraft.item.ItemStack;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.tileentity.EndPortalTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.CachedBlockInfo;
import net.minecraft.util.Direction;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.IBooleanFunction;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;
import net.minecraft.world.dimension.DimensionType;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

import javax.management.ReflectionException;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Random;

public class BlockElementalPortal extends Block {
    // TODO: model?
    protected static final VoxelShape SHAPE = Block.makeCuboidShape(0.0D, 0.0D, 0.0D, 16.0D, 12.0D, 16.0D);
    public static Block stored_end_portal = null;

    public BlockElementalPortal(Block.Properties p_i48406_1_) {
        super(p_i48406_1_);
        setRegistryName(Reference.MODID, "elemental_portal");

        try {
            reregisterEndPortal();
        } catch (Exception e) {

        }
    }

    private void reregisterEndPortal() throws Exception {
        if (stored_end_portal != null) return;
        stored_end_portal = Blocks.END_PORTAL;

        Field endPortal = Blocks.class.getDeclaredField("END_PORTAL");
        Field mods = Field.class.getDeclaredField("modifiers");
        mods.setAccessible(true);
        mods.set(endPortal, endPortal.getModifiers() &~ Modifier.FINAL);
        endPortal.set(null, this);
    }

    public TileEntity createNewTileEntity(IBlockReader p_196283_1_) {
        return new EndPortalTileEntity();
    }

    public VoxelShape getShape(BlockState p_220053_1_, IBlockReader p_220053_2_, BlockPos p_220053_3_, ISelectionContext p_220053_4_) {
        return SHAPE;
    }

    public void onEntityCollision(BlockState p_196262_1_, World p_196262_2_, BlockPos p_196262_3_, Entity p_196262_4_) {
        if (!p_196262_2_.isRemote && !p_196262_4_.isPassenger() && !p_196262_4_.isBeingRidden() && p_196262_4_.isNonBoss() && VoxelShapes.compare(VoxelShapes.create(p_196262_4_.getBoundingBox().offset((double)(-p_196262_3_.getX()), (double)(-p_196262_3_.getY()), (double)(-p_196262_3_.getZ()))), p_196262_1_.getShape(p_196262_2_, p_196262_3_), IBooleanFunction.AND)) {
            p_196262_4_.changeDimension(
                    p_196262_2_.dimension.getType() == Elements.inst.ELEMENTAL_JUNCTION_TYPE
                            ? DimensionType.OVERWORLD : Elements.inst.ELEMENTAL_JUNCTION_TYPE
            );
        }

    }

    @OnlyIn(Dist.CLIENT)
    public void animateTick(BlockState p_180655_1_, World p_180655_2_, BlockPos p_180655_3_, Random p_180655_4_) {
        double lvt_5_1_ = (double)((float)p_180655_3_.getX() + p_180655_4_.nextFloat());
        double lvt_7_1_ = (double)((float)p_180655_3_.getY() + 0.8F);
        double lvt_9_1_ = (double)((float)p_180655_3_.getZ() + p_180655_4_.nextFloat());
        double lvt_11_1_ = 0.0D;
        double lvt_13_1_ = 0.0D;
        double lvt_15_1_ = 0.0D;
        p_180655_2_.addParticle(ParticleTypes.SMOKE, lvt_5_1_, lvt_7_1_, lvt_9_1_, 0.0D, 0.0D, 0.0D);
    }

    @OnlyIn(Dist.CLIENT)
    public ItemStack getItem(IBlockReader p_185473_1_, BlockPos p_185473_2_, BlockState p_185473_3_) {
        return ItemStack.EMPTY;
    }
}
