package com.structbuilders.elements.blocks;

import com.google.common.base.Predicates;
import com.structbuilders.elements.Elements;
import com.structbuilders.elements.Reference;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.EndPortalFrameBlock;
import net.minecraft.block.material.Material;
import net.minecraft.block.material.MaterialColor;
import net.minecraft.block.pattern.BlockPattern;
import net.minecraft.block.pattern.BlockPatternBuilder;
import net.minecraft.block.pattern.BlockStateMatcher;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.EnderEyeItem;
import net.minecraft.item.ItemStack;
import net.minecraft.util.CachedBlockInfo;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

import javax.annotation.Nullable;

public class BlockCryingObsidian extends Block {
    public BlockCryingObsidian() {
        super(Block.Properties.create(Material.ROCK, MaterialColor.BLACK).hardnessAndResistance(50.0F, 1200.0F).lightValue(15));
        setRegistryName(Reference.MODID, "crying_obsidian");
    }


    private static BlockPattern portalShape;
    public static BlockPattern getOrCreatePortalShape() {
        if (portalShape == null) {
            portalShape = BlockPatternBuilder.start().aisle("?xxx?", "x???x", "x???x", "x???x", "?xxx?").where('?', CachedBlockInfo.hasState(BlockStateMatcher.ANY)).where('x', CachedBlockInfo.hasState(BlockStateMatcher.forBlock(Elements.CRYING_OBSIDIAN))).build();
        }

        return portalShape;
    }

//    @Override
//    public void onBlockPlacedBy(World world, BlockPos blockpos, BlockState p_180633_3_, @Nullable LivingEntity p_180633_4_, ItemStack p_180633_5_) {
//        super.onBlockPlacedBy(world, blockpos, p_180633_3_, p_180633_4_, p_180633_5_);
//        BlockPattern.PatternHelper blockpattern$patternhelper = BlockCryingObsidian.getOrCreatePortalShape().match(world, blockpos);
//        if (blockpattern$patternhelper != null) {
//            BlockPos blockpos1 = blockpattern$patternhelper.getFrontTopLeft().add(
//                    -3,
//                    0,
//                    -3);
//
//            System.out.println(blockpattern$patternhelper.getForwards());
//
//            for(int i = 0; i < 3; ++i) {
//                for(int j = 0; j < 3; ++j) {
//                    world.setBlockState(blockpos1.add(i * blockpattern$patternhelper.getForwards().getXOffset(), 0, j* blockpattern$patternhelper.getForwards().getZOffset()), Elements.ELEMENT_PORTAL.getDefaultState(), 2);
//                }
//            }
//
//            world.playBroadcastSound(1038, blockpos1.add(1, 0, 1), 0);
//        }
//    }
}
