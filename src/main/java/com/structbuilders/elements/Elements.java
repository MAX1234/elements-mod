package com.structbuilders.elements;

import com.mojang.datafixers.FunctionType;
import com.structbuilders.elements.blocks.BlockCryingObsidian;
import com.structbuilders.elements.blocks.BlockElementalPortal;
import com.structbuilders.elements.dimensions.elemental_junction.DimensionElementalJunction;
import com.sun.istack.internal.NotNull;
import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.material.MaterialColor;
import net.minecraft.client.renderer.color.ItemColors;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.Items;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.Biomes;
import net.minecraft.world.dimension.Dimension;
import net.minecraft.world.dimension.DimensionType;
import net.minecraft.world.dimension.NetherDimension;
import net.minecraftforge.common.DimensionManager;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.ModDimension;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.event.world.RegisterDimensionsEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.InterModComms;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.event.lifecycle.InterModEnqueueEvent;
import net.minecraftforge.fml.event.lifecycle.InterModProcessEvent;
import net.minecraftforge.fml.event.server.FMLServerStartingEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.GameData;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.nio.ByteBuffer;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

// The value here should match an entry in the META-INF/mods.toml file
@Mod(Reference.MODID)
@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD)
public class Elements {
    // Directly reference a log4j logger.
    public static final Block CRYING_OBSIDIAN = new BlockCryingObsidian();
    public static final Block ELEMENT_PORTAL = new BlockElementalPortal(Block.Properties.create(Material.PORTAL, MaterialColor.BLACK).doesNotBlockMovement().lightValue(15).hardnessAndResistance(-1.0F, 3600000.0F).noDrops());
    //    public static final DimensionType CLOUD_DIM_TYPE = DimensionType.register(CLOUD_NAME, "_"+CLOUD_NAME, CLOUD_DIM_ID, WorldProviderCloud.class, true);
    private static final Logger LOGGER = LogManager.getLogger();
    public DimensionType ELEMENTAL_JUNCTION_TYPE;
    public static Elements inst;

    public Elements() {

        // Register ourselves for server and other game events we are interested in
        MinecraftForge.EVENT_BUS.register(this);
        MinecraftForge.EVENT_BUS.addListener(this::registerDimensions);
        inst = this;
    }

    @SubscribeEvent
    public static void onBlocksRegistry(final RegistryEvent.Register<Block> blockRegistryEvent) {
        // register a new block here
        blockRegistryEvent.getRegistry().registerAll(
                CRYING_OBSIDIAN, ELEMENT_PORTAL
        );
    }

    @SubscribeEvent
    public static void registerItems(RegistryEvent.Register<Item> event) {
        event.getRegistry().registerAll(
                createItemBlockForBlock(CRYING_OBSIDIAN, new Item.Properties().group(ItemGroup.BUILDING_BLOCKS).maxStackSize(64))
        );
    }

    public static ModDimension dim;

    @SubscribeEvent
    public static void registerDimensions_(RegistryEvent.Register<ModDimension> event) {
        dim = new ModDimension() {
            @Override
            public BiFunction<World, DimensionType, ? extends Dimension> getFactory() {
                return new BiFunction<World, DimensionType, Dimension>() {
                    @Override
                    public Dimension apply(World world, DimensionType dimensionType) {
                        return new DimensionElementalJunction(world, dimensionType);
                    }
                };
            }
        }.setRegistryName(new ResourceLocation("elements", "elemental_junction"));

//        event.getRegistry().registerAll(
//                dim
//        );
    }

    public void registerDimensions(RegisterDimensionsEvent event) {
        System.out.print("DIMENSION REGISTER EVENT CALLED: ");
        System.out.println(event.getMissingNames());
        Elements.inst.ELEMENTAL_JUNCTION_TYPE = DimensionManager.registerDimension(
                new ResourceLocation("elements", "elemental_junction"),
                dim,
                null,
                false
        );
    }

    /**
     * Create the itemblock for the given block instance. The registryname will be set here.
     *
     * @param block      The block instance to create the item for.
     * @param properties The item properties to use
     * @return The itemblock
     */
    private static BlockItem createItemBlockForBlock(Block block, Item.Properties properties) {
        return (BlockItem) new BlockItem(block, properties).setRegistryName(block.getRegistryName());
    }
}
